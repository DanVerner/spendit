package com.ftnsoftware.spendit.category

import android.app.Activity
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase

class CategoryItemSwipe(adapter: CategoryItemAdapter, activity: Activity, db: AppDatabase?) :
    ItemTouchHelper.SimpleCallback(0, LEFT) {

    private val mActivity: Activity = activity
    private val mAdapter: CategoryItemAdapter = adapter
    private var mDb: AppDatabase? = db
    private val mContext = mActivity.applicationContext
    private val icon: Drawable?
    private val background: ColorDrawable

    init {
        icon = ContextCompat.getDrawable(
            mContext,
            R.drawable.ic_delete_white_32dp
        )
        background = ColorDrawable(Color.RED)
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return ItemTouchHelper.Callback.makeMovementFlags(0, LEFT)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        openConfirmDialog(position)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX / 81, dY, actionState, isCurrentlyActive)

        val itemView = viewHolder.itemView
        val backgroundCornerOffset = 20

        val iconMargin = (itemView.height - icon!!.intrinsicHeight) / 2
        val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
        val iconBottom = iconTop + icon.intrinsicHeight

        if (dX < 0) {
            val iconLeft = itemView.right - iconMargin - icon.intrinsicWidth
            val iconRight = itemView.right - iconMargin
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)

            background.setBounds(
                itemView.right + dX.toInt() - backgroundCornerOffset,
                itemView.top, itemView.right, itemView.bottom
            )
        } else {
            background.setBounds(0, 0, 0, 0)
        }

        background.draw(c)
        icon.draw(c)
    }

    private fun openConfirmDialog(position: Int) {
        val builder = AlertDialog.Builder(mActivity)
        builder.setTitle("Внимание!")
            .setIcon(R.drawable.ic_confirm_32dp)
            .setMessage(mActivity.getString(R.string.message_confirm_category_delete))
            .setCancelable(false)
            .setNegativeButton("Отмена") { dialogInterface, _ ->
                dialogInterface.cancel()
                mAdapter.notifyDataSetChanged()
            }
            .setPositiveButton("Да") { dialogInterface, _ -> mAdapter.deleteItem(mDb, position) }
        val alert = builder.create()
        alert.show()
    }

}