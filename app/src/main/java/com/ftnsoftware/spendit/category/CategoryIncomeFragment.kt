package com.ftnsoftware.spendit.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.*
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.interfaces.IAddTransaction

class CategoryIncomeFragment(activity: CategoryActivity, listener: IAddTransaction) : Fragment() {

    private var mDb: AppDatabase? = null
    private val mListener = listener
    private val mActivity = activity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_category_list, container, false)
        mDb = AppDatabase.getInstance(activity!!.applicationContext)
        val recycler = view.findViewById<RecyclerView>(R.id.list_category)
        val layoutManager = LinearLayoutManager(activity!!.applicationContext)
        val listIncome = Methods.getIncomeTypes(mDb)
        val adapter = CategoryItemAdapter(listIncome, mListener)
        val itemSwipe = CategoryItemSwipe(adapter, mActivity, mDb)
        val itemTouchHelper = ItemTouchHelper(itemSwipe)
        itemTouchHelper.attachToRecyclerView(recycler)

        recycler.layoutManager = layoutManager
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.addItemDecoration(DividerItemDecoration(activity!!.applicationContext, 1))
        recycler.adapter = adapter

        adapter.notifyDataSetChanged()

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        AppDatabase.destroyInstance()
    }
}