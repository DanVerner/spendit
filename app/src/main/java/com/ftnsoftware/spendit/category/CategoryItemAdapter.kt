package com.ftnsoftware.spendit.category

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.interfaces.IAddTransaction

class CategoryItemAdapter(categories: ArrayList<Category>, listener: IAddTransaction) :
    RecyclerView.Adapter<CategoryItemAdapter.CategoryViewHolder>() {

    private var mCategories = categories
    private var mListener: IAddTransaction? = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)

        return CategoryViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return mCategories.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = mCategories[holder.adapterPosition]

        holder.category.text = category.name

        if (!category.isIncome)
            holder.type.setImageDrawable(holder.context.getDrawable(R.drawable.ic_arrow_outcome_red_24dp))
    }

    inner class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val type: ImageView = view.findViewById(R.id.image_item_type)
        val category: TextView = view.findViewById(R.id.text_item_category)
        val context: Context = view.context
    }

    fun deleteItem(db: AppDatabase?, position: Int) {
        val deletedItem = mCategories[position]
        deleteTransaction(db, deletedItem.name)
        mCategories.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun deleteTransaction(db: AppDatabase?, name: String) {
        val categoryDataDao = db?.categoryDataDao()
        categoryDataDao?.deleteCategory(name)
        mListener?.onDialogClickListener()
    }
}