package com.ftnsoftware.spendit.category

data class Category (
    val name: String,
    val isIncome: Boolean
)