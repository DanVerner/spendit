package com.ftnsoftware.spendit.category

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ftnsoftware.spendit.interfaces.IAddTransaction

class CategoryTabAdapter(fragmentManager: FragmentManager, activity: CategoryActivity, listener: IAddTransaction) :
    FragmentPagerAdapter(fragmentManager) {

    private val mListener = listener
    private val mActivity = activity

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                CategoryIncomeFragment(mActivity, mListener)
            }
            else -> {
                CategoryOutcomeFragment(mActivity, mListener)
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }
}