package com.ftnsoftware.spendit.category

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout

class CategoryActivity : AppCompatActivity(), IAddTransaction {

    var mFragmentAdapter: CategoryTabAdapter? = null
    var mViewCategory: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)

        val toolbar: Toolbar = findViewById(R.id.toolbar_category)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        title = applicationContext.getString(R.string.item_category)

        val db = AppDatabase.getInstance(applicationContext)
        val fab: FloatingActionButton = findViewById(R.id.fab_add_category)
        val tabLayout: TabLayout = findViewById(R.id.tab_category)
        mFragmentAdapter = CategoryTabAdapter(supportFragmentManager, this, this)
        mViewCategory = findViewById(R.id.view_category)
        mViewCategory?.adapter = mFragmentAdapter
        mViewCategory?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab) {}

            override fun onTabUnselected(p0: TabLayout.Tab) {}

            override fun onTabSelected(p0: TabLayout.Tab) {
                mViewCategory?.currentItem = p0.position
            }
        })

        fab.setOnClickListener {
            CategoryDialog.display(supportFragmentManager, db)
        }
    }

    override fun onDialogClickListener() {
        mViewCategory?.adapter = mFragmentAdapter
    }
}