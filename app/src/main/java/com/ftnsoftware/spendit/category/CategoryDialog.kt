package com.ftnsoftware.spendit.category

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.db.entities.CategoryData
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import com.google.android.material.textfield.TextInputLayout

class CategoryDialog : DialogFragment() {

    private var mToolbar: Toolbar? = null
    private var mListener: IAddTransaction? = null
    private var mIsIncome = false

    companion object {

        private var mDb: AppDatabase? = null

        fun display(fragmentManager: FragmentManager, db: AppDatabase?): CategoryDialog {
            val categoryDialog = CategoryDialog()
            categoryDialog.show(fragmentManager, "CategoryDialog")
            mDb = db

            return categoryDialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullscreenDialog)

        if (savedInstanceState != null)
            mIsIncome = savedInstanceState.getBoolean("isIncome")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mListener = context as IAddTransaction
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog

        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
            dialog.window?.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_add_category, container, false)
        mToolbar = view.findViewById(R.id.toolbar_add_category)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val switchCategoryIncome = view.findViewById<RadioButton>(R.id.radio_income_category)
        val switchCategoryOutcome = view.findViewById<RadioButton>(R.id.radio_outcome_category)
        val layoutCategory = view.findViewById<TextInputLayout>(R.id.layout_category_name)
        val editCategory = view.findViewById<EditText>(R.id.edit_category_name)

        switchCategoryOutcome.isChecked = true

        switchCategoryIncome.setOnClickListener { mIsIncome = true }
        switchCategoryOutcome.setOnClickListener { mIsIncome = false }

        mToolbar?.setNavigationOnClickListener { dismiss() }
        mToolbar?.title = resources.getText(R.string.action_add)
        mToolbar?.setTitleTextColor(Color.WHITE)
        mToolbar?.inflateMenu(R.menu.menu_add_dialog)
        mToolbar?.setOnMenuItemClickListener {
            val category: Category

            if (editCategory.text.isNotEmpty()) {
                category = Category(editCategory.text.toString(), mIsIncome)

                if (!isDuplicateCategory(category)) {
                    saveCategoryToDb(category)
                    mListener?.onDialogClickListener()
                    dismiss()
                } else
                    layoutCategory?.error = resources.getText(R.string.error_category_exists)
            } else
                layoutCategory?.error = resources.getText(R.string.error_field_is_empty)

            true
        }
    }

    private fun isDuplicateCategory(category: Category): Boolean {
        val categoryDataDao = mDb?.categoryDataDao()
        val categoryFromDb = categoryDataDao?.getSpecificCategory(category.name, category.isIncome)

        if (categoryFromDb != null) {
            return categoryFromDb.toLowerCase() == category.name.toLowerCase()
        }

        return false
    }

    private fun saveCategoryToDb(category: Category) {
        val categoryDataDao = mDb?.categoryDataDao()
        val categoryData = CategoryData(category)
        categoryDataDao?.insertCategory(categoryData)
    }
}