package com.ftnsoftware.spendit.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ftnsoftware.spendit.transaction.Transaction

@Entity(tableName = "transactions")
class TransactionData (
    @PrimaryKey var id: String,
    @Embedded var transaction: Transaction
)