package com.ftnsoftware.spendit.db.dao

import androidx.room.*
import com.ftnsoftware.spendit.db.entities.TransactionData
import com.ftnsoftware.spendit.transaction.Transaction
import java.util.*

@Dao
interface TransactionDataDao {

    @Query("select * from transactions")
    fun getAllTransactions(): List<Transaction>

    @Query("select * from transactions where date >= :firstDayOfMonth and date <= :lastDayOfMonth")
    fun getFilteredTransactions(firstDayOfMonth: Date?, lastDayOfMonth: Date?): List<Transaction>

    @Query("select * from transactions where isIncome = :isIncome")
    fun getSpecificTransaction(isIncome: Boolean?): List<Transaction>

    @Query("select * from transactions where isIncome = :isIncome and date >= :firstDayOfMonth and date <= :lastDayOfMonth")
    fun getSpecificFilteredTransaction(
        isIncome: Boolean?,
        firstDayOfMonth: Date?,
        lastDayOfMonth: Date?
    ): List<Transaction>

    @Insert
    fun insertTransaction(transactionData: TransactionData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateTransaction(transactionData: TransactionData)

    @Query("delete from transactions where uid = :uid")
    fun deleteTransaction(uid: String)

    @Query("delete from transactions")
    fun deleteAllTransactions()
}