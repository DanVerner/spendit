package com.ftnsoftware.spendit.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ftnsoftware.spendit.db.dao.CategoryDataDao
import com.ftnsoftware.spendit.db.dao.TransactionDataDao
import com.ftnsoftware.spendit.db.entities.CategoryData
import com.ftnsoftware.spendit.db.entities.TransactionData

@Database(entities = [TransactionData::class, CategoryData::class], version = 7)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun transactionDataDao() : TransactionDataDao
    abstract fun categoryDataDao() : CategoryDataDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if(INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "spenditDB"
                    ).fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}