package com.ftnsoftware.spendit.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ftnsoftware.spendit.category.Category

@Entity(tableName = "transaction_categories")
class CategoryData(
    @PrimaryKey
    @Embedded var transactionCategory: Category
)