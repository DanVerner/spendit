package com.ftnsoftware.spendit.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ftnsoftware.spendit.db.entities.CategoryData
import com.ftnsoftware.spendit.category.Category

@Dao
interface CategoryDataDao {

    @Query("select * from transaction_categories where isIncome = :isIncome")
    fun getCategory(isIncome: Boolean): List<Category>

    @Query("select name from transaction_categories where name = :categoryName and isIncome = :isIncome")
    fun getSpecificCategory(categoryName: String, isIncome: Boolean): String

    @Insert
    fun insertCategory(categoryData: CategoryData)

    @Query("delete from transaction_categories where name = :name")
    fun deleteCategory(name: String)

    @Query("delete from transaction_categories")
    fun deleteAllCategories()
}