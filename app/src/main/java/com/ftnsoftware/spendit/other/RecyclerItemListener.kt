package com.ftnsoftware.spendit.other

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerItemListener(context: Context, rv: RecyclerView, listener: RecyclerTouchListener) :
    RecyclerView.OnItemTouchListener {

    private val mListener: RecyclerTouchListener
    private val mDetector: GestureDetector

    interface RecyclerTouchListener {
        fun onClickItem(v: View?, position: Int)

        fun onLongClickItem(v: View?, position: Int)
    }

    init {
        mListener = listener
        mDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onLongPress(e: MotionEvent) {
//                val v = rv.findChildViewUnder(e.x, e.y)
//                listener.onLongClickItem(v, rv.getChildAdapterPosition(v!!))
            }

            override fun onSingleTapUp(e: MotionEvent): Boolean {
                val v = rv.findChildViewUnder(e.x, e.y)
                listener.onClickItem(v, rv.getChildAdapterPosition(v!!))
                return true
            }
        })
    }

    override fun onInterceptTouchEvent(recyclerView: RecyclerView, motionEvent: MotionEvent): Boolean {
        val child = recyclerView.findChildViewUnder(motionEvent.x, motionEvent.y)
        return child != null && mDetector.onTouchEvent(motionEvent)
    }

    override fun onTouchEvent(recyclerView: RecyclerView, motionEvent: MotionEvent) {

    }

    override fun onRequestDisallowInterceptTouchEvent(b: Boolean) {

    }
}