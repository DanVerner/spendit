package com.ftnsoftware.spendit.other

import android.text.Spanned
import android.text.method.DigitsKeyListener
import java.util.regex.Pattern
import android.text.TextUtils

class DecimalDigitsFilter(digitsBefore: Int?, digitsAfter: Int?) : DigitsKeyListener() {

    private val mDigitsBeforeZero: Int
    private val mDigitsAfterZero: Int
    private val mPattern: Pattern

    init {
        this.mDigitsBeforeZero = digitsBefore ?: DIGITS_BEFORE_ZERO_DEFAULT
        this.mDigitsAfterZero = digitsAfter ?: DIGITS_AFTER_ZERO_DEFAULT
        mPattern = Pattern.compile(
            "-?[0-9]{0," + mDigitsBeforeZero + "}+((\\.[0-9]{0," + mDigitsAfterZero
                    + "})?)||(\\.)?"
        )
    }

    companion object {
        private const val DIGITS_BEFORE_ZERO_DEFAULT = 100
        private const val DIGITS_AFTER_ZERO_DEFAULT = 100
    }

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val replacement = source.subSequence(start, end).toString()
        val newVal = (dest.subSequence(0, dstart).toString() + replacement
                + dest.subSequence(dend, dest.length).toString())
        val matcher = mPattern.matcher(newVal)
        if (matcher.matches())
            return null

        return if (TextUtils.isEmpty(source))
            dest.subSequence(dstart, dend)
        else
            ""
    }
}