package com.ftnsoftware.spendit.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ftnsoftware.spendit.MainActivity
import com.ftnsoftware.spendit.R
import com.github.paolorotolo.appintro.AppIntro

class IntroActivity : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(IntroFragment.newInstance(R.layout.onboarding_intro1))
//        addSlide(IntroFragment.newInstance(R.layout.onboarding_intro2))

    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        openMainActivity()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        openMainActivity()
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun openPinActivity(){

    }
}