package com.ftnsoftware.spendit.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class IntroFragment : Fragment() {

    companion object {
        fun newInstance(layoutId: Int) : IntroFragment {
            val introFragment = IntroFragment()
            val args = Bundle()
            args.putInt("layoutId", layoutId)
            introFragment.arguments = args

            return introFragment
        }
    }

    private var layoutId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey("layoutId"))
            layoutId = arguments!!.getInt("layoutId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }
}