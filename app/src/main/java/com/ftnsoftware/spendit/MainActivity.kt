package com.ftnsoftware.spendit

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.category.CategoryActivity
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.transaction.*
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import com.ftnsoftware.spendit.onboarding.IntroActivity
import java.util.*
import kotlin.collections.ArrayList
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.security.SecurityActivity
import com.whiteelephant.monthpicker.MonthPickerDialog


class MainActivity : AppCompatActivity(), ITransaction, IAddTransaction,
    NavigationView.OnNavigationItemSelectedListener {

    val REQUEST_CODE = 1

    val mTransactions = ArrayList<Transaction>()
    var adapter: TransactionCardAdapter? = null
    private var mDb: AppDatabase? = null
    lateinit var mTransactionPresenter: TransactionPresenter

    var mFirstDayOfMonth: Date? = null
    var mLastDayOfMonth: Date? = null
    var mFab: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar_main)
        setSupportActionBar(toolbar)
        setFilterButton(toolbar)
        checkFirstLogin()
        val isLogged = intent.getBooleanExtra("logged", false)

        if (!isLogged)
            checkSecureLogin()

        mDb = AppDatabase.getInstance(applicationContext)
        mTransactionPresenter = TransactionPresenter(this)
        mFirstDayOfMonth = mTransactionPresenter.getTodayFirstDay()
        mLastDayOfMonth = mTransactionPresenter.getTodayLastDay()
        mTransactionPresenter.getTransactionsFromDB(mDb, "general", mFirstDayOfMonth, mLastDayOfMonth)

        mFab = findViewById(R.id.btn_add_transaction)
        mFab?.setOnClickListener { view ->
            TransactionDialog.display(supportFragmentManager, mDb, "general")
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    private fun setFilterButton(toolbar: Toolbar) {
        val filterBtn = Button(this)
        val layoutParams = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        layoutParams.gravity = Gravity.END
        filterBtn.layoutParams = layoutParams
        filterBtn.setBackgroundColor(Color.TRANSPARENT)
        filterBtn.setTextColor(Color.WHITE)
        filterBtn.text = Methods.parseDateToStringFilter(Date())
        toolbar.addView(filterBtn)
        onFilterBtnClick(filterBtn)
    }

    private fun onFilterBtnClick(filterBtn: Button) {
        filterBtn.setOnClickListener {
            openMonthFilterDialog(filterBtn)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        AppDatabase.destroyInstance()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mTransactionPresenter.getTransactionsFromDB(mDb, "general", mFirstDayOfMonth, mLastDayOfMonth)
        }
    }

    override fun showTransactions(
        transactionsTree: TreeMap<Date, ArrayList<Transaction>>,
        transactions: ArrayList<Transaction>
    ) {
//        mTransactions.addAll(transactions)
        val transactionCards = ArrayList<TransactionCard>()

        for (key in transactionsTree.descendingKeySet()) {
            transactionCards.add(TransactionCard(Methods.parseDateToString(key), transactionsTree[key]!!))
        }

        adapter = TransactionCardAdapter(this, this, transactionCards, mDb)

        val recycler = findViewById<RecyclerView>(R.id.list_general_transactions)
        val layoutManager = LinearLayoutManager(applicationContext)
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 || dy < 0 && mFab!!.isShown)
                    mFab?.hide()
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    mFab?.show()

                super.onScrollStateChanged(recyclerView, newState)
            }
        })
        recycler.layoutManager = layoutManager
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.adapter = adapter

        adapter?.notifyDataSetChanged()

        val amountIncome = mTransactionPresenter.countAllIncome(transactions)
        val amountOutcome = mTransactionPresenter.countAllOutcome(transactions)
        val amountBalance = mTransactionPresenter.countBalance(amountIncome, amountOutcome)

        val textIncome = findViewById<TextView>(R.id.text_income_summary)
        val textOutcome = findViewById<TextView>(R.id.text_outcome_summary)
        val textBalance = findViewById<TextView>(R.id.text_balance_summary)

        textIncome.text = getString(R.string.item_transaction_value, Methods.parseDoubleToString(amountIncome))
        textOutcome.text = getString(R.string.item_transaction_value, Methods.parseDoubleToString(amountOutcome))
        textBalance.text = getString(R.string.item_transaction_value, Methods.parseDoubleToString(amountBalance))
    }

    override fun onDialogClickListener() {
        mTransactionPresenter.getTransactionsFromDB(mDb, "general", mFirstDayOfMonth, mLastDayOfMonth)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_income -> {
                openIncomeWindow("income")
            }
            R.id.nav_outcome -> {
                openIncomeWindow("outcome")
            }
            R.id.nav_summary -> {
                openIncomeWindow("general")
            }
            R.id.nav_rate -> {
                launchRate()
            }
            R.id.nav_about -> {
                openAboutDialog()
            }
            R.id.nav_categories -> {
                openCategoryWindow()
            }
            R.id.nav_security -> {
                openSecurityActivity()
            }
            R.id.nav_delete_all -> {
                openClearDatabaseConfirmDialog()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun checkFirstLogin() {
        val preferences = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE)
        val isFirstLogin = preferences.getBoolean("isFirstLogin", true)

        if (isFirstLogin) {
            val editor = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE).edit()
            editor.putBoolean("isFirstLogin", false)
            editor.apply()

            openIntroActivity()
        }
    }

    private fun checkSecureLogin() {
        val preferences = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE)
        val isSecuritySet = preferences.getBoolean("securitySet", false)

        if (isSecuritySet){
            val securityIntent = Intent(this, SecurityActivity::class.java)
            securityIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            securityIntent.putExtra("activityType", "login")
            startActivity(securityIntent)
            finish()
        }
    }

    private fun openIntroActivity() {
        val introIntent = Intent(this, IntroActivity::class.java)
        startActivity(introIntent)
    }

    private fun openIncomeWindow(tag: String) {
        val incomeIntent = Intent(this, TransactionActivity::class.java)
        incomeIntent.putExtra("type", tag)
        startActivityForResult(incomeIntent, REQUEST_CODE)
    }

    private fun openCategoryWindow() {
        val categoryIntent = Intent(this, CategoryActivity::class.java)
        startActivity(categoryIntent)
    }


    private fun openSecurityActivity() {
        val preferences = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE)
        val isSecuritySet = preferences.getBoolean("securitySet", false)

        val securityIntent = Intent(this, SecurityActivity::class.java)

        if (!isSecuritySet)
            securityIntent.putExtra("activityType", "setup")
        else
            securityIntent.putExtra("activityType", "action")

        startActivity(securityIntent)
    }

    private fun openMonthFilterDialog(filterBtn: Button) {
        val calendar = Calendar.getInstance()

        val builder = MonthPickerDialog.Builder(
            this,
            MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
                val newMonth = selectedMonth + 1
                val month: String = if (newMonth < 10)
                    "0$newMonth"
                else
                    "$newMonth"

                val firstDayOfMonth = Methods.parseStringToDatePrg("01.$month.$selectedYear")
                val lastDayOfMonth = mTransactionPresenter.getLastDayOfMonth("01/$month/$selectedYear")
                filterBtn.text = Methods.parseDateToStringFilter(firstDayOfMonth)
                mTransactionPresenter.getTransactionsFromDB(mDb, "general", firstDayOfMonth, lastDayOfMonth)
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
        )

        builder.setTitle("Выберите промежуток")
            .setActivatedMonth(calendar.get(Calendar.MONTH))
            .setActivatedYear(calendar.get(Calendar.YEAR))
            .setOnMonthChangedListener { }
            .setOnYearChangedListener { }
            .build()
            .show()
    }

    private fun openClearDatabaseConfirmDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Внимание!")
            .setIcon(R.drawable.ic_confirm_32dp)
            .setMessage(getString(R.string.message_confirm_clear_database))
            .setCancelable(false)
            .setNegativeButton("Отмена") { dialogInterface, _ ->
                dialogInterface.cancel()
            }
            .setPositiveButton("Да") { dialogInterface, _ ->
                mTransactionPresenter.deleteAllTransactionsFromDB(mDb)
                mTransactionPresenter.getTransactionsFromDB(mDb, "general", mFirstDayOfMonth, mLastDayOfMonth)
            }
        val alert = builder.create()
        alert.show()
    }

    private fun openAboutDialog() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.dialog_about, null)
        val btnRate = dialogView.findViewById<Button>(R.id.button_rate)
        val textVersion = dialogView.findViewById<TextView>(R.id.text_about_version)

        textVersion.text = getString(R.string.about_version, BuildConfig.VERSION_NAME)
        btnRate.setOnClickListener { launchRate() }
        builder.setView(dialogView).setCancelable(true)

        val alert = builder.create()
        alert.show()
    }

    private fun launchRate() {
        val uri = Uri.parse("market://details?id=$packageName")
        val startMarket = Intent(Intent.ACTION_VIEW, uri)

        try {
            startActivity(startMarket)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, R.string.message_cannot_start_market, Toast.LENGTH_LONG).show()
        }
    }
}
