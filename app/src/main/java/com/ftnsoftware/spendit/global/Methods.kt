package com.ftnsoftware.spendit.global

import com.ftnsoftware.spendit.category.Category
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.app.Activity
import android.app.AlertDialog
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.db.entities.CategoryData
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols


class Methods {
    companion object {
        fun parseDateToString(date: Date): String {
            val dateFormat = SimpleDateFormat("d MMM yyyy", Locale.getDefault())
            return dateFormat.format(date)
        }

        fun parseDateToStringFilter(date: Date): String {
            val dateFormat = SimpleDateFormat("MMM yyyy", Locale.getDefault())
            return dateFormat.format(date)
        }

        fun parseStringToDate(date: String): Date {
            return SimpleDateFormat("d MMM yyyy", Locale.getDefault()).parse(date)
        }

        fun parseStringToDatePrg(date: String): Date {
            return SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).parse(date)
        }

        fun getIncomeTypes(db: AppDatabase?): ArrayList<Category> {
            val incomeTypes = ArrayList<Category>()
            val categoryDataDao = db?.categoryDataDao()
            incomeTypes.addAll(categoryDataDao!!.getCategory(true))

            if (incomeTypes.isEmpty()) {
                incomeTypes.add(Category("Зарплата", true))
                incomeTypes.add(Category("Сбережения", true))
                incomeTypes.add(Category("Продажа", true))
                incomeTypes.add(Category("Вложение", true))
                incomeTypes.add(Category("Пассивный доход", true))

                for (i in 0 until incomeTypes.size) {
                    val categoryData = CategoryData(incomeTypes[i])
                    categoryDataDao.insertCategory(categoryData)
                }
            }

            return incomeTypes
        }

        fun getOutcomeTypes(db: AppDatabase?): ArrayList<Category> {
            val outcomeTypes = ArrayList<Category>()
            val categoryDataDao = db?.categoryDataDao()
            outcomeTypes.addAll(categoryDataDao!!.getCategory(false))

            if (outcomeTypes.isEmpty()) {
                outcomeTypes.add(Category("Еда", false))
                outcomeTypes.add(Category("Продукты", false))
                outcomeTypes.add(Category("Транспорт", false))
                outcomeTypes.add(Category("Медикаменты", false))
                outcomeTypes.add(Category("Телефон", false))
                outcomeTypes.add(Category("Одежда", false))
                outcomeTypes.add(Category("Жилье", false))
                outcomeTypes.add(Category("Развлечения", false))
                outcomeTypes.add(Category("Спорт", false))
                outcomeTypes.add(Category("Такси", false))

                for (i in 0 until outcomeTypes.size) {
                    val categoryData = CategoryData(outcomeTypes[i])
                    categoryDataDao.insertCategory(categoryData)
                }
            }

            return outcomeTypes
        }

        fun openErrorDialog(activity: Activity, message: String) {
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Внимание!")
                .setIcon(R.drawable.ic_warning_32dp)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("Ок") { dialogInterface, i ->
                    dialogInterface.cancel()
                }
            val alert = builder.create()
            alert.show()
        }

        fun parseDoubleToString(double: Double): String {
            val decimalFormat = DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
            decimalFormat.maximumFractionDigits = 340

            return decimalFormat.format(double)
        }
    }
}