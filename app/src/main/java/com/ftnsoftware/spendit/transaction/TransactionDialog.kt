package com.ftnsoftware.spendit.transaction

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.category.Category
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.db.entities.TransactionData
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import com.ftnsoftware.spendit.other.DecimalDigitsFilter
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.collections.ArrayList

class TransactionDialog : DialogFragment() {

    private var mToolbar: Toolbar? = null
    private var mTextDate: TextView? = null
    private var mListener: IAddTransaction? = null
    private var dateString = ""
    private var mSelectedType = ""
    private var isIncome = false

    companion object {

        private var mType = ""
        private var mDb: AppDatabase? = null
        private var mTransaction: Transaction? = null

        fun display(
            fragmentManager: FragmentManager,
            db: AppDatabase?,
            type: String,
            transaction: Transaction? = null
        ): TransactionDialog {
            val incomeDialog = TransactionDialog()
            incomeDialog.show(fragmentManager, "TransactionDialog")
            mType = type
            mDb = db

            if (transaction != null)
                mTransaction = transaction

            return incomeDialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullscreenDialog)
        if (savedInstanceState != null)
            isIncome = savedInstanceState.getBoolean("isIncome")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mListener = context as IAddTransaction
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog

        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
            dialog.window?.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_add_item, container, false)
        mToolbar = view.findViewById(R.id.toolbar)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var uid = UUID.randomUUID().toString()

        val selectedDateStr = Methods.parseDateToString(Date())
        var selectedDate = Methods.parseStringToDate(selectedDateStr)
        var types = ArrayList<Category>()
        var selectedTransactionCategory: Category? = null


        val groupIncome = view.findViewById<RadioGroup>(R.id.group_switch)
        val switchIncome = view.findViewById<RadioButton>(R.id.radio_income)
        val switchOutcome = view.findViewById<RadioButton>(R.id.radio_outcome)
        val btnAddDate = view.findViewById<MaterialButton>(R.id.btn_add_date)
        mTextDate = view.findViewById(R.id.text_income_date)
        val editComment = view.findViewById<EditText>(R.id.edit_income_comment)
        val editAmount = view.findViewById<EditText>(R.id.edit_income_amount)
        val selectType = view.findViewById<Spinner>(R.id.spinner_select_type)
        val layoutAmount = view.findViewById<TextInputLayout>(R.id.layout_income_amount)

        val incomeCategories = Methods.getIncomeTypes(mDb)
        val outcomeCategories = Methods.getOutcomeTypes(mDb)

        editAmount.filters = arrayOf<InputFilter>(DecimalDigitsFilter(20, 2))

        when (mType) {
            "income" -> {
                isIncome = true
                types = incomeCategories
            }
            "outcome" -> {
                isIncome = false
                types = outcomeCategories
            }
            "general" -> {
                groupIncome.visibility = View.VISIBLE
                if (isIncome) {
                    switchIncome.isChecked = true
                    types = incomeCategories
                } else {
                    switchOutcome.isChecked = true
                    types = outcomeCategories
                }
            }
            "edit" -> {
                if (mTransaction != null) {
                    groupIncome.visibility = View.VISIBLE
                    isIncome = mTransaction!!.isIncome

                    if (isIncome) {
                        switchIncome.isChecked = true
                        types = incomeCategories
                    } else {
                        switchOutcome.isChecked = true
                        types = outcomeCategories
                    }

                    uid = mTransaction!!.uid
                    val type = mTransaction!!.type
                    selectedTransactionCategory = Category(type, isIncome)
                    selectedDate = mTransaction!!.date
                    mTextDate?.text = Methods.parseDateToString(selectedDate)
                    editComment.setText(mTransaction!!.name)
                    editAmount.setText(Methods.parseDoubleToString(mTransaction!!.amount))
                }
            }
        }

        switchIncome.setOnClickListener {
            isIncome = true
            types = incomeCategories
            createTypeSpinner(types, selectType, null)
        }
        switchOutcome.setOnClickListener {
            isIncome = false
            types = outcomeCategories
            createTypeSpinner(types, selectType, null)
        }

        mToolbar?.setNavigationOnClickListener { dismiss() }
        mToolbar?.title = resources.getText(R.string.action_add)
        mToolbar?.setTitleTextColor(Color.WHITE)
        mToolbar?.inflateMenu(R.menu.menu_add_dialog)
        mToolbar?.setOnMenuItemClickListener {
            val transaction: Transaction
            if (dateString != "")
                selectedDate = Methods.parseStringToDatePrg(dateString)

            if (editAmount?.text!!.isNotEmpty()) {
                transaction = Transaction(
                    uid,
                    selectedDate,
                    editComment?.text.toString(),
                    mSelectedType,
                    editAmount.text.toString().toDouble(),
                    isIncome
                )

                if (mType == "edit")
                    updateTransactionInDB(transaction)
                else
                    saveTransactionToDB(transaction)

                mListener?.onDialogClickListener()

                dismiss()
            } else {
                layoutAmount?.error = resources.getText(R.string.error_field_is_empty)
            }
            true
        }

        mTextDate?.text = Methods.parseDateToString(selectedDate)

        btnAddDate?.setOnClickListener {
            onCreateDialog().show()
        }

        createTypeSpinner(types, selectType, selectedTransactionCategory)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("isIncome", isIncome)
        super.onSaveInstanceState(outState)
    }

    private fun createTypeSpinner(types: ArrayList<Category>, selectType: Spinner, transactionCategory: Category?) {
        val arrayAdapter = TransactionTypeSpinnerAdapter(context, types)
        selectType.adapter = arrayAdapter
        if (transactionCategory != null) {
            val position = arrayAdapter.getPosition(transactionCategory)
            selectType.setSelection(position)
        }
        selectType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                val selectedTypeObj = arrayAdapter.getSelectedType(position)
                mSelectedType = selectedTypeObj.name
            }
        }
    }

    private fun saveTransactionToDB(transaction: Transaction) {
        val transactionDataDao = mDb?.transactionDataDao()
        val transactionData = TransactionData(transaction.uid, transaction)
        transactionDataDao?.insertTransaction(transactionData)
    }

    private fun updateTransactionInDB(transaction: Transaction) {
        val transactionDataDao = mDb?.transactionDataDao()
        val transactionData = TransactionData(transaction.uid, transaction)
        transactionDataDao?.updateTransaction(transactionData)
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private fun onCreateDialog(): Dialog {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        return DatePickerDialog(context, getDateCallback, year, month, day)
    }

    private var getDateCallback: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

            val newMonth = monthOfYear + 1
            val month: String = if (newMonth < 10)
                "0$newMonth"
            else
                "$newMonth"

            dateString = "$dayOfMonth.$month.$year"
            val date = Methods.parseStringToDatePrg(dateString)
            val dateStr = Methods.parseDateToString(date)
            mTextDate?.text = dateStr
        }
}