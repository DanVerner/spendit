package com.ftnsoftware.spendit.transaction

data class TransactionCard(
    val date: String,
    val transactions: ArrayList<Transaction>
)