package com.ftnsoftware.spendit.transaction

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.interfaces.IAddTransaction

class TransactionItemAdapter(transaction: ArrayList<Transaction>, listener: IAddTransaction) :
    RecyclerView.Adapter<TransactionItemAdapter.CustomViewHolder>() {

    private var mIncome = transaction
    private var mListener: IAddTransaction? = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false)

        return CustomViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return mIncome.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val income = mIncome[holder.adapterPosition]
        holder.incomeCategory.text = income.type

        if (income.isIncome)
            holder.incomeAmount.text =
                holder.context.getString(R.string.item_transaction_income, Methods.parseDoubleToString(income.amount))
        else {
            holder.incomeAmount.text =
                holder.context.getString(R.string.item_transaction_outcome, Methods.parseDoubleToString(income.amount))
            holder.incomeType.setImageDrawable(holder.context.getDrawable(R.drawable.ic_arrow_outcome_red_24dp))
        }
    }

    inner class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var incomeCategory: TextView = view.findViewById(R.id.text_item_category)
        var incomeAmount: TextView = view.findViewById(R.id.text_item_amount)
        var incomeType: ImageView = view.findViewById(R.id.image_item_type)
        var context: Context = view.context
    }

    fun deleteItem(db: AppDatabase?, position: Int) {
        val deletedItem = mIncome[position]
        deleteTransaction(db, deletedItem.uid)
        mIncome.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun deleteTransaction(db: AppDatabase?, uid: String) {
        val transactionDataDao = db?.transactionDataDao()
        transactionDataDao?.deleteTransaction(uid)
        mListener?.onDialogClickListener()
    }
}