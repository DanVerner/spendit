package com.ftnsoftware.spendit.transaction

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.category.Category

class TransactionTypeSpinnerAdapter(context: Context?, private var transactionCategory: ArrayList<Category>) :
    BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ItemRowHolder

        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_type, parent, false)
            viewHolder = ItemRowHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ItemRowHolder
        }

        viewHolder.typeName.text = transactionCategory[position].name

        return view
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return transactionCategory.size
    }

    fun getPosition(value: Category?): Int {
        return transactionCategory.indexOf(value)
    }

    fun getSelectedType(position: Int): Category {
        return transactionCategory[position]
    }

    private class ItemRowHolder(row: View?) {
        val typeName: TextView = row?.findViewById(R.id.text_income_type_name) as TextView
    }
}