package com.ftnsoftware.spendit.transaction

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Transaction(
    val uid: String,
    val date: Date,
    val name: String,
    val type: String,
    val amount: Double,
    val isIncome: Boolean
) : Parcelable