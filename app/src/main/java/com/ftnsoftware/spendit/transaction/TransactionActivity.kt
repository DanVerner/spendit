package com.ftnsoftware.spendit.transaction

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import kotlin.collections.ArrayList
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.whiteelephant.monthpicker.MonthPickerDialog
import java.util.*

class TransactionActivity : AppCompatActivity(), ITransaction, IAddTransaction {

    private val mTransactions = ArrayList<Transaction>()
    private val mNewTransactions = ArrayList<Transaction>()
    private var mAdapter: TransactionCardAdapter? = null
    private var mTransactionType = ""

    lateinit var mTransactionPresenter: TransactionPresenter
    private var mDb: AppDatabase? = null

    var mFirstDayOfMonth: Date? = null
    var mLastDayOfMonth: Date? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_income)
        val toolbar: Toolbar = findViewById(R.id.toolbar_transaction)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        mDb = AppDatabase.getInstance(applicationContext)
        mTransactionPresenter = TransactionPresenter(this)
        mTransactionType = intent.getStringExtra("type")

        setActivityTitle(mTransactionType)

        val btnAddIncome = findViewById<FloatingActionButton>(R.id.btn_add_income)

        if (mTransactionType == "general")
            mTransactionPresenter.getAllTransactionsFromDB(mDb)
        else {
            setFilterButton(toolbar)
            mFirstDayOfMonth = mTransactionPresenter.getTodayFirstDay()
            mLastDayOfMonth = mTransactionPresenter.getTodayLastDay()
            mTransactionPresenter.getTransactionsFromDB(mDb, mTransactionType, mFirstDayOfMonth, mLastDayOfMonth)
        }

        openAddIncomeDialog(btnAddIncome, mTransactionType)
    }

    private fun setFilterButton(toolbar: Toolbar) {
        val filterBtn = Button(this)
        val layoutParams = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        layoutParams.gravity = Gravity.END
        filterBtn.layoutParams = layoutParams
        filterBtn.setBackgroundColor(Color.TRANSPARENT)
        filterBtn.setTextColor(Color.WHITE)
        filterBtn.text = Methods.parseDateToStringFilter(Date())
        toolbar.addView(filterBtn)
        onFilterBtnClick(filterBtn)
    }

    private fun onFilterBtnClick(filterBtn: Button) {
        filterBtn.setOnClickListener {
            openMonthFilterDialog(filterBtn)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        AppDatabase.destroyInstance()
    }

    override fun onBackPressed() {
        val bundle = Bundle()
        val returnIntent = Intent()
        bundle.putSerializable("listTransactions", mNewTransactions)
        returnIntent.putExtras(bundle)
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
    }

    override fun showTransactions(
        transactionsTree: TreeMap<Date, ArrayList<Transaction>>,
        transactions: ArrayList<Transaction>
    ) {

        val transactionCards = ArrayList<TransactionCard>()

        for (key in transactionsTree.descendingKeySet()) {
            transactionCards.add(TransactionCard(Methods.parseDateToString(key), transactionsTree[key]!!))
        }

        mAdapter = TransactionCardAdapter(this, this, transactionCards, mDb)

        val recycler = findViewById<RecyclerView>(R.id.list_income)
        val layoutManager = LinearLayoutManager(applicationContext)

        recycler.layoutManager = layoutManager
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.adapter = mAdapter

        mAdapter?.notifyDataSetChanged()

        val textIncomeSummary = findViewById<TextView>(R.id.text_income_summary)
        val textOutcomeSummary = findViewById<TextView>(R.id.text_outcome_summary)
        val textBalanceSummary = findViewById<TextView>(R.id.text_balance_summary)
        val incomeSummary: Double
        val outcomeSummary: Double
        val balanceSummary: Double

        when (mTransactionType) {
            "general" -> {
                incomeSummary = mTransactionPresenter.countAllIncome(transactions)
                outcomeSummary = mTransactionPresenter.countAllOutcome(transactions)
                balanceSummary = mTransactionPresenter.countBalance(incomeSummary, outcomeSummary)

                textIncomeSummary.text =
                    getString(R.string.item_transaction_value, Methods.parseDoubleToString(incomeSummary))
                textOutcomeSummary.text =
                    getString(R.string.item_transaction_value, Methods.parseDoubleToString(outcomeSummary))
                textBalanceSummary.text =
                    getString(R.string.item_transaction_value, Methods.parseDoubleToString(balanceSummary))
            }
            "income" -> {
                incomeSummary = mTransactionPresenter.countAllIncome(transactions)

                textIncomeSummary.text =
                    getString(R.string.item_transaction_value, Methods.parseDoubleToString(incomeSummary))
                textOutcomeSummary.text = getString(R.string.item_transaction_value, "0.00")
                textBalanceSummary.text = getString(R.string.item_transaction_value, "0.00")
            }
            "outcome" -> {
                outcomeSummary = mTransactionPresenter.countAllOutcome(transactions)

                textIncomeSummary.text = getString(R.string.item_transaction_value, "0.00")
                textOutcomeSummary.text =
                    getString(R.string.item_transaction_value, Methods.parseDoubleToString(outcomeSummary))
                textBalanceSummary.text = getString(R.string.item_transaction_value, "0.00")
            }
        }


    }

    private fun openAddIncomeDialog(btnAddIncome: FloatingActionButton?, type: String) {
        btnAddIncome?.setOnClickListener {
            TransactionDialog.display(supportFragmentManager, mDb, type)
        }
    }

    override fun onDialogClickListener() {
        mTransactionPresenter.getTransactionsFromDB(mDb, mTransactionType, mFirstDayOfMonth, mLastDayOfMonth)
    }

    private fun openMonthFilterDialog(filterBtn: Button) {
        val calendar = Calendar.getInstance()

        val builder = MonthPickerDialog.Builder(
            this,
            MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
                val newMonth = selectedMonth + 1
                val month: String = if (newMonth < 10)
                    "0$newMonth"
                else
                    "$newMonth"

                val firstDayOfMonth = Methods.parseStringToDatePrg("01.$month.$selectedYear")
                val lastDayOfMonth = mTransactionPresenter.getLastDayOfMonth("01/$month/$selectedYear")
                filterBtn.text = Methods.parseDateToStringFilter(firstDayOfMonth)
                mTransactionPresenter.getTransactionsFromDB(mDb, mTransactionType, firstDayOfMonth, lastDayOfMonth)
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
        )

        builder.setTitle("Выберите промежуток")
            .setActivatedMonth(calendar.get(Calendar.MONTH))
            .setActivatedYear(calendar.get(Calendar.YEAR))
            .setOnMonthChangedListener { }
            .setOnYearChangedListener { }
            .build()
            .show()
    }

    private fun setActivityTitle(type: String) {
        when (type) {
            "income" -> title = "Доходы"
            "outcome" -> title = "Расходы"
            "general" -> title = "Общие"
        }
    }
}