package com.ftnsoftware.spendit.transaction

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.*
import com.ftnsoftware.spendit.R
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.other.RecyclerItemListener
import androidx.recyclerview.widget.RecyclerView
import com.ftnsoftware.spendit.global.Methods
import com.ftnsoftware.spendit.interfaces.IAddTransaction
import java.math.BigDecimal

class TransactionCardAdapter(
    activity: Activity,
    listener: IAddTransaction,
    transactionCards: ArrayList<TransactionCard>,
    db: AppDatabase?
) : RecyclerView.Adapter<TransactionCardAdapter.CustomViewHolder>() {

    private val mListener = listener
    private val mActivity = activity
    private var mTransactionCards = transactionCards
    private var mDb = db

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.item_transaction_card, parent, false)

        return CustomViewHolder(cardView)
    }

    override fun getItemCount(): Int {
        return mTransactionCards.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val transactionCard = mTransactionCards[holder.adapterPosition]
        val transactions = transactionCard.transactions
        var totalOutcomeDec = BigDecimal("0.00")

        for (i in 0 until transactions.size) {
            if (!transactions[i].isIncome) {
                val bigDecimal = BigDecimal(Methods.parseDoubleToString(transactions[i].amount))
                totalOutcomeDec += bigDecimal
            }
        }

        val totalOutcome = totalOutcomeDec.toDouble()

        if (totalOutcome > 0.0) {
            holder.outcomeValue.text =
                holder.context.getString(R.string.item_transaction_value, Methods.parseDoubleToString(totalOutcome))
            holder.outcomeLabel.visibility = View.VISIBLE
            holder.outcomeValue.visibility = View.VISIBLE
        }

        holder.transactionDate.text = transactionCard.date

        val adapter = TransactionItemAdapter(transactions, mListener)
        val recycler = holder.transactionsList
        val layoutManager = LinearLayoutManager(holder.context)
        val itemSwipe = TransactionItemSwipe(adapter, mActivity, mDb)
        val itemTouchHelper = ItemTouchHelper(itemSwipe)
        itemTouchHelper.attachToRecyclerView(recycler)

        recycler.layoutManager = layoutManager
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.addItemDecoration(DividerItemDecoration(holder.context, 1))
        recycler.adapter = adapter
        recycler.addOnItemTouchListener(
            RecyclerItemListener(
                holder.context,
                recycler,
                object : RecyclerItemListener.RecyclerTouchListener {
                    override fun onClickItem(v: View?, position: Int) {
                        TransactionDialog.display(
                            (holder.context as AppCompatActivity).supportFragmentManager,
                            mDb,
                            "edit",
                            transaction = transactions[position]
                        )
                    }

                    override fun onLongClickItem(v: View?, position: Int) {}

                })
        )

        adapter.notifyDataSetChanged()
    }

    inner class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var transactionDate: TextView = view.findViewById(R.id.text_card_date)
        var transactionsList: RecyclerView = view.findViewById(R.id.list_date_transactions)
        var outcomeLabel: TextView = view.findViewById(R.id.text_outcome_label)
        var outcomeValue: TextView = view.findViewById(R.id.text_outcome_value)
        val context: Context = view.context

    }
}