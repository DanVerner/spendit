package com.ftnsoftware.spendit.transaction

import java.util.*
import kotlin.collections.ArrayList

interface ITransaction {
    fun showTransactions(transactionsTree: TreeMap<Date, ArrayList<Transaction>>, transactions: ArrayList<Transaction>)
}