package com.ftnsoftware.spendit.transaction

import android.util.Log
import com.ftnsoftware.spendit.db.AppDatabase
import com.ftnsoftware.spendit.global.Methods
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TransactionPresenter(transaction: ITransaction) {

    private val mTransaction: ITransaction = transaction

    fun getTransactionsFromDB(db: AppDatabase?, type: String, firstDayOfMonth: Date?, lastDayOfMonth: Date?) {

        var isIncome: Boolean? = null
        val transactionDataDao = db?.transactionDataDao()
        val transactions = ArrayList<Transaction>()

        if (type == "general") {
            transactions.addAll(transactionDataDao!!.getFilteredTransactions(firstDayOfMonth, lastDayOfMonth))
            Log.d("getTransactions", "transactions in database ${transactions.size}")
            sortTransactions(transactions)
        } else {
            if (type == "income")
                isIncome = true
            else if (type == "outcome")
                isIncome = false
            transactions.addAll(
                transactionDataDao!!.getSpecificFilteredTransaction(
                    isIncome,
                    firstDayOfMonth,
                    lastDayOfMonth
                )
            )
            Log.d("getTransactions", "transactions in database ${transactions.size}")
            sortTransactions(transactions)
        }
    }

    fun getAllTransactionsFromDB(db: AppDatabase?) {
        val transactionDataDao = db?.transactionDataDao()
        val transactions = ArrayList<Transaction>()

        transactions.addAll(transactionDataDao!!.getAllTransactions())
        Log.d("getTransactions", "transactions in database ${transactions.size}")

        sortTransactions(transactions)

    }

    fun deleteAllTransactionsFromDB(db: AppDatabase?) {
        val transactionDataDao = db?.transactionDataDao()
        transactionDataDao?.deleteAllTransactions()
    }

    fun countAllIncome(transactions: ArrayList<Transaction>): Double {
        var amountIncome = BigDecimal("0.00")

        for (i in 0 until transactions.size) {
            if (transactions[i].isIncome) {
                val bigDecimal = BigDecimal(Methods.parseDoubleToString(transactions[i].amount))
                amountIncome += bigDecimal
            }
        }

        return amountIncome.toDouble()
    }

    fun countAllOutcome(transactions: ArrayList<Transaction>): Double {
        var amountOutcome = BigDecimal("0.00")

        for (i in 0 until transactions.size) {
            if (!transactions[i].isIncome) {
                val bigDecimal = BigDecimal(Methods.parseDoubleToString(transactions[i].amount))
                amountOutcome += bigDecimal
            }
        }

        return amountOutcome.toDouble()
    }

    fun countBalance(amountIncome: Double, amountOutcome: Double): Double {
        val income = BigDecimal(Methods.parseDoubleToString(amountIncome))
        val outcome = BigDecimal(Methods.parseDoubleToString(amountOutcome))
        return income.subtract(outcome).toDouble()
    }

    fun sortTransactions(transactions: ArrayList<Transaction>) {

        val transactionTree = TreeMap<Date, ArrayList<Transaction>>()
        var date: Date? = null

        for (i in 0 until transactions.size) {

            val dailyTransactions = ArrayList<Transaction>()

            if (transactions[i].date != date) {
                date = transactions[i].date

                for (j in 0 until transactions.size) {
                    if (date == transactions[j].date)
                        dailyTransactions.add(transactions[j])
                }

                transactionTree[date] = dailyTransactions
            }
        }

        mTransaction.showTransactions(transactionTree, transactions)

    }

    fun getLastDayOfMonth(date: String): Date {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val convertedDate = dateFormat.parse(date)

        val calendar = Calendar.getInstance()
        calendar.time = convertedDate
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)
        calendar.set(Calendar.SECOND, 59)

        return calendar.time
    }

    fun getTodayFirstDay(): Date {
        val calendar = Calendar.getInstance()
        val todayDate = Date()

        calendar.time = todayDate
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        return calendar.time
    }

    fun getTodayLastDay(): Date {
        val calendar = Calendar.getInstance()
        val todayDate = Date()

        calendar.time = todayDate

        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)
        calendar.set(Calendar.SECOND, 59)

        return calendar.time
    }
}