package com.ftnsoftware.spendit.security

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.ftnsoftware.spendit.MainActivity
import com.ftnsoftware.spendit.R
import com.google.android.material.textfield.TextInputLayout

class SecurityActivity : AppCompatActivity() {

    private lateinit var mLayoutLogin: View
    private lateinit var mLayoutActions: View
    private lateinit var mLayoutChange: View
    private var mSecurityCode: String? = null
    private var mSecurityWord: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security)
        initViews()
        mSecurityCode = getSecurityCode()
        mSecurityWord = getSecurityWord()

        when (intent.getStringExtra("activityType")) {
            "login" -> {
                initLoginViews()
                mLayoutLogin.visibility = View.VISIBLE
            }
            "action" -> {
                initActionViews()
                mLayoutActions.visibility = View.VISIBLE
            }
            "setup" -> {
                initChangeViews("setup")
                mLayoutChange.visibility = View.VISIBLE
            }
        }

    }

    private fun initViews() {
        mLayoutLogin = findViewById(R.id.layout_login)
        mLayoutActions = findViewById(R.id.layout_choose_action)
        mLayoutChange = findViewById(R.id.layout_set_password)
    }

    private fun initLoginViews() {
        val layForgotPass: ConstraintLayout = findViewById(R.id.layout_forgot_pwd)
        val tilPassword: TextInputLayout = findViewById(R.id.til_pwd_login)
        val tilKeyword: TextInputLayout = findViewById(R.id.til_kwd_login)
        val editLogin: EditText = findViewById(R.id.edit_pwd_login)
        val editKeyword: EditText = findViewById(R.id.edit_kwd_login)
        val textForgot: TextView = findViewById(R.id.text_forgot_pass)
        val textRemindedCode: TextView = findViewById(R.id.text_kwd_number)
        val btnLogin: Button = findViewById(R.id.btn_login)
        val btnRemind: Button = findViewById(R.id.btn_get_pwd)

        var countLogin = 0
        var countRemind = 0

        editLogin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilPassword.isErrorEnabled = false
            }

        })

        editKeyword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilKeyword.isErrorEnabled = false
            }

        })

        textForgot.setOnClickListener {
            layForgotPass.visibility = View.VISIBLE
            textForgot.visibility = View.GONE
        }

        btnLogin.setOnClickListener {
            val code = editLogin.text.toString().trim()
            if (code.isEmpty())
                tilPassword.error = getString(R.string.error_field_is_empty)
            else {
                if (code == mSecurityCode)
                    openMainActivity()
                else {
                    tilPassword.error = getString(R.string.error_pass_incorrect)
                    if (countLogin >= 4) {
                        editLogin.text = null
                        tilPassword.isEnabled = false
                        btnLogin.isEnabled = false
                    }
                    countLogin++
                }
            }
        }

        btnRemind.setOnClickListener {
            val keyword = editKeyword.text.toString().trim()
            if (keyword.isEmpty())
                tilKeyword.error = getString(R.string.error_field_is_empty)
            else {
                if (keyword == mSecurityWord) {
                    textRemindedCode.text = getString(R.string.message_password, mSecurityCode)
                    textRemindedCode.visibility = View.VISIBLE
                    tilPassword.isEnabled = true
                    btnLogin.isEnabled = true
                } else {
                    tilKeyword.error = getString(R.string.error_security_word_incorrect)
                    if (countRemind >= 4) {
                        editKeyword.text = null
                        tilKeyword.isEnabled = false
                        btnRemind.isEnabled = false
                    }
                    countRemind++
                }
            }
        }
    }

    private fun initActionViews() {
        val btnChangePass: Button = findViewById(R.id.btn_change_pass)
        val btnChangeKeyword: Button = findViewById(R.id.btn_change_keyword)
        val btnClearSecurity: Button = findViewById(R.id.btn_clear_security)

        btnChangePass.setOnClickListener { initChangeViews("pass") }
        btnChangeKeyword.setOnClickListener { initChangeViews("keyword") }
        btnClearSecurity.setOnClickListener { initChangeViews("clear") }
    }

    private fun initChangeViews(action: String) {
        val tilFirst: TextInputLayout = findViewById(R.id.til_pwd_1)
        val tilSecond: TextInputLayout = findViewById(R.id.til_pwd_2)
        val tilThird: TextInputLayout = findViewById(R.id.til_pwd_3)
        val tilForth: TextInputLayout = findViewById(R.id.til_pwd_4)
        val editFirst: EditText = findViewById(R.id.edit_pwd_1)
        val editSecond: EditText = findViewById(R.id.edit_pwd_2)
        val editThird: EditText = findViewById(R.id.edit_pwd_3)
        val editForth: EditText = findViewById(R.id.edit_pwd_4)
        val textLabel: TextView = findViewById(R.id.label_secret_message)
        val btnApply: Button = findViewById(R.id.btn_set_security)

        when (action) {
            "pass" -> {
                tilSecond.hint = getString(R.string.edit_pass_new)
                tilFirst.visibility = View.GONE
                tilForth.visibility = View.GONE
                textLabel.visibility = View.GONE
                mLayoutActions.visibility = View.GONE
                mLayoutChange.visibility = View.VISIBLE
            }
            "keyword" -> {
                tilFirst.hint = getString(R.string.edit_keyword_old)
                tilSecond.hint = getString(R.string.edit_keyword_new)
                tilThird.hint = getString(R.string.edit_pass_input)
                editFirst.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                editSecond.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                editThird.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
                tilForth.visibility = View.GONE
                textLabel.visibility = View.GONE
                mLayoutActions.visibility = View.GONE
                mLayoutChange.visibility = View.VISIBLE
            }
            "clear" -> {
                tilSecond.visibility = View.GONE
                tilForth.visibility = View.GONE
                textLabel.visibility = View.GONE
                btnApply.text = getString(R.string.action_delete)
                mLayoutActions.visibility = View.GONE
                mLayoutChange.visibility = View.VISIBLE
            }
        }

        editFirst.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilFirst.isErrorEnabled = false
            }
        })

        editSecond.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilSecond.isErrorEnabled = false
            }
        })

        editThird.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilThird.isErrorEnabled = false
            }
        })

        editForth.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tilForth.isErrorEnabled = false
            }
        })

        btnApply.setOnClickListener {
            when (action) {
                "pass" -> {
                    val newPassword = editSecond.text.toString().trim()
                    val keyword = editThird.text.toString().trim()

                    if (newPassword.isEmpty())
                        tilSecond.error = getString(R.string.error_field_is_empty)
                    if (keyword.isEmpty())
                        tilThird.error = getString(R.string.error_field_is_empty)

                    if (newPassword.isNotEmpty() && keyword.isNotEmpty()) {
                        if (keyword == mSecurityWord) {
                            val editor = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE).edit()
                            editor.putString("securityCode", newPassword)
                            editor.apply()

                            Toast.makeText(this, "Новый пароль установлен", Toast.LENGTH_LONG).show()
                            finish()
                        } else
                            tilThird.error = getString(R.string.error_security_word_incorrect)
                    }
                }
                "keyword" -> {
                    val oldKeyword = editFirst.text.toString().trim()
                    val newKeyword = editSecond.text.toString().trim()
                    val password = editThird.text.toString().trim()

                    if (oldKeyword.isEmpty())
                        tilFirst.error = getString(R.string.error_field_is_empty)
                    if (newKeyword.isEmpty())
                        tilSecond.error = getString(R.string.error_field_is_empty)
                    if (password.isEmpty())
                        tilThird.error = getString(R.string.error_field_is_empty)

                    if (oldKeyword.isNotEmpty() && newKeyword.isNotEmpty() && password.isNotEmpty()) {
                        if (oldKeyword == mSecurityWord && password == mSecurityCode) {
                            val editor = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE).edit()
                            editor.putString("securityWord", newKeyword)
                            editor.apply()

                            Toast.makeText(this, "Новое секретное слово установлено", Toast.LENGTH_LONG).show()
                            finish()
                        } else {
                            textLabel.text = getString(R.string.error_wrong_pass_or_word)
                            textLabel.setTextColor(Color.RED)
                            textLabel.visibility = View.VISIBLE
                        }
                    }

                }
                "clear" -> {
                    val password = editFirst.text.toString().trim()
                    val keyword = editThird.text.toString().trim()

                    if (password.isEmpty())
                        tilFirst.error = getString(R.string.error_field_is_empty)
                    if (keyword.isEmpty())
                        tilThird.error = getString(R.string.error_field_is_empty)

                    if (password.isNotEmpty() && keyword.isNotEmpty()){
                        if (password == mSecurityCode && keyword == mSecurityWord){
                            val editor = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE).edit()
                            editor.putBoolean("securitySet", false)
                            editor.putString("securityCode", "")
                            editor.putString("securityWord", "")
                            editor.apply()

                            Toast.makeText(this, "Данные удалены", Toast.LENGTH_LONG).show()
                            finish()
                        }
                    }
                }
                "setup" -> {
                    val password = editFirst.text.toString().trim()
                    val passwordConfirm = editSecond.text.toString().trim()
                    val keyword = editThird.text.toString().trim()
                    val keywordConfirm = editForth.text.toString().trim()

                    if (password.isEmpty())
                        tilFirst.error = getString(R.string.error_field_is_empty)
                    if (passwordConfirm.isEmpty())
                        tilSecond.error = getString(R.string.error_field_is_empty)
                    if (keyword.isEmpty())
                        tilThird.error = getString(R.string.error_field_is_empty)
                    if (keywordConfirm.isEmpty())
                        tilForth.error = getString(R.string.error_field_is_empty)

                    if (password.isNotEmpty() && passwordConfirm.isNotEmpty() && keyword.isNotEmpty() && keywordConfirm.isNotEmpty()) {
                        if ((password == passwordConfirm) && (keyword == keywordConfirm)) {
                            val editor = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE).edit()
                            editor.putBoolean("securitySet", true)
                            editor.putString("securityCode", password)
                            editor.putString("securityWord", keyword)
                            editor.apply()

                            Toast.makeText(this, "Пароль установлен", Toast.LENGTH_LONG).show()
                            finish()
                        } else {
                            if (password != passwordConfirm)
                                tilSecond.error = getString(R.string.error_pass_dont_match)
                            if (keyword != keywordConfirm)
                                tilForth.error = getString(R.string.error_keywords_dont_match)
                        }
                    }
                }
            }
        }
    }

    private fun getSecurityCode(): String? {
        val preferences = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE)
        return preferences.getString("securityCode", "")
    }

    private fun getSecurityWord(): String? {
        val preferences = getSharedPreferences("ApplicationData", Context.MODE_PRIVATE)
        return preferences.getString("securityWord", "")
    }

    private fun openMainActivity() {
        val mainIntent = Intent(this, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        mainIntent.putExtra("logged", true)
        startActivity(mainIntent)
        finish()
    }
}